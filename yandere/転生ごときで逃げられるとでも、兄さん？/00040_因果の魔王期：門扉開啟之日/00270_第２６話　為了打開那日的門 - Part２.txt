蓋恩·麥克唐奈忘不了。



面對大恩師遺骨時的遺憾。

立在一片白骨上的不中用。



以及，在連那些都感覺不到的崩壞的時刻。



注意到的時候，一切都結束了。

原以為自己被八頭竜打倒了，到此為止了。



守護民眾是騎士的責任。

是將弱者庇護在盾後的騎士。



被那麼被教導，想著應該那麼做。

然後，打算理所當然地打算實踐。



可是，７年前的那一天。

一切都被打碎了。

應當守護的東西哪裡都沒有，有的只是一片白骨的世界――

――攜帶的誇張盾牌，變成了無法向舊友伸出援手的無用長物。



想要讓那一天重來。

不知道這麼祈願了多少次。



你到底、拯救了多少性命。

你到底、阻止了多少死亡。





告訴我啊，傑克·利巴。



我的盾、守護不了你嗎？





――在空中游動的鯨魚，高高地向天空噴水。

――在大雨中，那個巨影悠然自得。

――天和水的支配者，以母親的巨軀迎擊。





――第七巨獸。

――悠然母鯨《羅比艾爾》。





「這次一定，要用這面盾阻止人們受災」



蓋恩的大盾，急劇增加重量。



「就算成不了對那一天的贖罪―――即使這樣我也要前進。為了拾取沒能拾回的生命！！」







◆◆◆―――――――◆◆◆―――――――◆◆◆







露比·伯格森忘不了。



在那個地下迷宮知道的自己的無力。

到達不了的自己的手。



以及，兩者被擺在眼前的那一天。



想著所有事都可以自己做。

不需要夥伴、搭檔。

到底，人最終還是不得不自己想辦法。

如果指望別人，就會在這裡被別人絆住腳步。



就算相互利用，也不會合作。

就算親密交往，也不會合謀。

就這樣，她在貧民窟和精靈術師學院這兩個戰場上活了下來。



想著這樣就好。

想著這樣就好了吧。



但是，那一天。

７年前。

發生了自己的手觸及不到的事態。



撿了自己的師傅死了。

友人的一人死了。

學院整個崩壞了。

同班同學的一人不知道消失到何處去了。



所有的一切，都在她不知道的地方不明理由地發生。

不可能防禦得了。

不可能有辦法。



……啊啊，隨便吧。

我的手到達不了，就是說對我無所謂。

明明應該是這樣的。



那是自我欺騙。

事件之後，發現自己受到巨大打擊的露比，被迫承認自己的自我欺騙。



自己只是害怕而已。

害怕失去、害怕輸給比自己弱的人，所以想一個人呆著。



簡直就像在那個雨日顫抖的小貓。

她只是頑固地否定了對方伸出的手。





――在兩個陽光的照耀下，閃耀著黃金光輝。

――突入天空的那個威容。

――黃金巨人泰然而立，擋住勇者的去路。





――第六巨獸。

――燦然偽神《格魯特甘特》。





「沒有欺騙沒有辯解」



露比的周圍塗滿偽物的世界。



「推心置腹地說話――就像那晚的闘術場一樣吶」







◆◆◆―――――――◆◆◆―――――――◆◆◆







亞澤麗雅·奧斯汀忘不了。



和他度過的日子。

和她度過的日子。



以及，（同時）失去這兩者的那一天。



自己一定很天真吧。

優柔寡斷、模棱兩可、半途而廢。

是個稍有才能就得意忘形的孩子。



所以，注意到的時候就已經很憧憬了。

一心凝視著什麼的他的眼瞳。

全心全意支持他的她的樣子。



……啊啊，所以，從一開始就是空想。

在沒有自覺的時候，就承認自己輸了。

後來才意識到這就是我頑固否定戀愛的原因。



有生以來初次遇見的喜歡的人。

有生以來初次認識的真心的朋友。



有著半吊子才能、被讚揚這種才能的人環繞，紙老虎的我多虧了他們兩人總算撐住了。

如果是那兩人的話肯定沒問題……第一次放出青炎的我這麼想著。



但為什麼。

只留下我？



胸中空著的兩人份的大洞，７年的程度無法填補。

如今也還空著。

兩個人的地方好好地空著喲。　





――漆黑的巨軀是從未見過的炎獄。

――白色的雙眸是永遠的無謬。

――崇高的野獸，款待終焉的來訪。





――第五巨獸。

――僭越神狼《芬考爾》。





「你們（現在）在哪裡都好」



如清晨健碩般的青炎，蒸發了傾盆大雨。



「不管多遲――你們的歸宿、都在這裡！！」







◆◆◆―――――――◆◆◆―――――――◆◆◆







埃爾維斯·昆茨·溫莎忘不了。



與他相合的劍的重量。

與她說話時胸口的甜蜜。



以及，不認為那很糟糕的自己。



一切都是為了成為最強的王。

繼承了亡母的思念，埃爾維斯為此戰鬥、學習、進入了精靈術學院。



該怎麼描述第一次看到他的戰鬥時的心情呢。

想著能進入這個學院真是太好了。

想著這樣就能變得更強。

在自己追求的最強的道路上，確實有著他的身影。



實際戰鬥時，埃爾維斯輸了。

是無可狡辯的完敗。

那證明了自己還有變得更強的餘地――

意味著自己有著實現理想的可能性。



……那時候。

天才王子、所有人都這麼稱呼我。

但是，現在想想的話，沒有比那時候更閒的時期了。

從母親那裡繼承的思念到底有沒有達成，我自己才是那個最不明白的人。



沐浴眾多稱讚。

那些都是不負責任的東西，埃爾維斯對此感到空虛。

然而，只有她的稱讚不同。

沒有任何抵抗地，進入了自己心中。



……連我自己都覺得簡單。

只是稍微被表揚了一下，就受到了巨大的衝擊。

這一定是因為那是突然襲擊。

她從來只注視著他，我從來沒想過自己會被表揚――

那個破綻，一下子被戳到了。



可是，真是不可思議。

從那之後，目光追隨著她的日子一天天持續――

然而，卻完全沒有浮現慾望一樣的東西。



如果是亞澤麗雅的話，肯定會明白吧。

埃爾維斯用目光追著她。

喜歡上了喜歡他的她。



……真是的，你們太過分了。

只把我們玩弄於股掌之間，然後就消失了什麼的。



所以，不允許贏了就跑。

那一天，那扇門前方到底發生了什麼？

為什麼他不得不成為魔王呢？

為什麼她――不得不死呢？





――覆蓋天空的五彩之翼。

――比太陽更像太陽的光的化身。

――即使在雨中，不死的怪鳥也會照亮世界。





――第二巨獸。

――永恆怪鳥《格魯・特拉斯》。





「撒，結束吧」



只有埃爾維斯身邊，雨水飛向天空。



「７年的初戀、太長了」







◆◆◆―――――――◆◆◆―――――――◆◆◆







然後，傑克·利巴忘不了。



那個手的觸感。

那個脖子的觸感。

她的最後的臉。

她的最後的聲音。



你的聲音。

你的話語。

對妹妹你的憎惡。

對妹妹你的詛咒。



對妹妹你的。

對妹妹你的。

對妹妹你的。

對妹妹你的。

對妹妹你的。



忘不了、忘不了、忘不了。

忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了。

忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了忘不了！





絕對。



忘不了。







［距離浄化的太陽爆炸、還有約２小時４５分鐘］


fin
