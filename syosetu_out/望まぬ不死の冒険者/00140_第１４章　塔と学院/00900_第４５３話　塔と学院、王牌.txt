肉烤焦的味道和打雷後特有的刺激性怪味在周圍瀰漫開來

混合著漫天的煙塵，著實令人不快
在這樣的慘狀之中⋯⋯

那傢伙不知怎樣了呢
雖然還不清楚結果，但我實在不覺得，有什麼能承受了那種攻擊後還安然無恙的存在

「⋯⋯險些嗝屁吶」

在千鈞一髮之際，我們總算是逃離了魔術的效果範圍
奧格利的話，無疑是我們現在共同的感想

若是被那一擊命中，就算是我這副不死者的身軀，也難免灰飛煙滅吧
雖然感覺不到疼痛，但我的生存本能卻感到了恐怖
那個很不妙，這樣子，真的會完蛋的，向我發出了警報

雖說是不死者，姑且也算是活著的吧⋯⋯？

所謂活著，是指什麼，這種哲學性的問題在我腦海中盤旋起來，但現在不是考慮這些問題的時候，我趕快轉換了思維

「據說，熟練的魔術師足以匹敵一軍，看看這副樣子，總覺得能夠理解呢」
「一軍麼⋯⋯雖然感覺不是湊齊一軍就能解決的程度吶，剛才的魔術，無論哪一招都聞所未聞」
「羅蕾露知道一些古代魔術，剛才那些魔術，大概都是吧。說是因為魔力消耗不是一般的大，沒什麼使用的機會⋯⋯但會產生這種後果的話，就算不考慮魔力消耗的問題，也根本沒法拿來用吧」

老巨人經過的路徑上，樹木被拔起、折斷、碾碎，留下了相當可怕的痕跡，但羅蕾露這邊更是有過之而無不及
雖然一直把老巨人當作是怪物，但我們小隊裡，也有個堪稱怪物的老師呢

⋯⋯好像不止一個

說起來，我也是怪物來的
常識人只有奧格利一個麼⋯⋯

總覺得有點對不起他，嘛，本來就是因為他接下了那些委託，才會發展成現在這種狀況的
算了，先不考慮這些

「⋯⋯但是，不知道效果如何呢。雖然怎麼想也不可能沒事⋯⋯」

奧格利這樣說著，一臉嚴肅地盯向老巨人的方向
雷電引燃了周圍的樹木，冒出濃煙，現在視野還不太清楚
雖然覺得應該是幹掉了吧⋯⋯

從漸漸消散的煙塵中現身的，是某種巨大的，被燒得焦黑的物體
可能是因為其過於巨大，沒能整個烤焦，但雷電還是再上面留下了明顯的痕跡，外表面大部分都變成了黑色和棕色
之前刺入身體的鐵槍，可能因為羅蕾露的魔力耗盡，已經消失不見

老巨人的身體上可以看到幾處穿刺型的傷口，傷口裡也冒著黑煙
看上去，體內也被雷電傷到了吧⋯⋯大概
雖說是如此巨大，如此超越常理的存在，終究還是生物
被雷電貫穿全身的話⋯⋯也不會輕鬆吧

「⋯⋯看樣子，是幹掉了麼？」

奧格利一邊這樣說著，一邊向老巨人接近
我也小心翼翼跟在他身後
雖然覺得他不太可能突然跳起來，但我們還是不由得慢下腳步，握緊了手中的劍
慢慢接近，試著用劍刺一下

⋯⋯沒有動靜

這樣確認之後，奧格利回過頭來

「⋯⋯看來是成功了，總算結束了麼⋯⋯」

在他呼出一口氣的瞬間

「⋯⋯奧格利！！」

伴隨著一陣旋風，老巨人的手臂抬了起來
我拽緊奧格利，迅速拉開了距離

「⋯⋯唔噢噢噢噢！！」

老巨人發出一陣不成人聲的咆哮，掙扎著撐起了焦黑的身體
先抬起了上半身，然後手撐地面，用力站起身來

「⋯⋯竟然還能⋯⋯！！」

看到老巨人的身影，奧格利驚得目瞪口呆
實際上也確實很驚人啊
承受了那樣的攻擊後，竟然還能站起身來，這已經不僅僅是體力很好，耐久力很高的程度了
不愧是真正脫離常識的怪物

「⋯⋯但果然，還是傷得不輕吶」

我放開奧格利，重新冷靜觀察起老巨人，他的動作果然遲緩了許多
雖然拚命站了起來，但全身都在發出咔吧咔吧的聲音

羅蕾露的雷擊確實造成了巨大的傷害
但即便如此，老巨人還是站了起來
燒焦的臉上布滿血絲的眼睛，捕捉到了我們的身影，但是，他一句話也沒有說，只是發出模糊的呻吟聲，向我們走了過來
他也已經沒有一絲餘裕了

「奧格利！還能上嗎！？」
「隨時都行！只是⋯⋯怎麼樣才能打倒他啊」
「⋯⋯我來做，我還留有王牌，應該沒問題。不果，不知道劍能不能承受得住，所以只能使用一次」

老巨人發起了攻擊，我們一邊閃避，一邊商量了起來

我所說的王牌，就是指那個
雖然我現在的劍是聖氣、魔力和氣都能通過的特製品，但同時注入三種力量的話，果然還是承受不住吧
聖魔氣融合術本身，具有使命中的東西被向內壓縮的效果
這種效果應該也影響到了武器本身吧
所以盡可能不想使出這一招啊，但此時不用，更待何時呢
雖然使出這招也不一定能起到作用，但總比坐以待斃強上百倍

即使註定要輸，也要使出全部招數以後再輸
當然，我並沒有要輸的打算
如果真的沒有作用，我應該會撒腿就跑吧

老巨人毫髮無傷的時候，我們應該是逃不掉的，但現在就不一樣了
受到了這種程度的傷害，不認為他還會拚命追趕逃跑的對手
就算真的追上來，我也有自信能逃掉

問題在於菲麗絲他們的村子，但把村民們疏散掉就好了
或者把《哥布林》和《塞壬》作為人質，迫使對方讓步。感覺這種做法像壊人一樣，但現在也不是挑三揀四的時候

比起這些事情，還是專註於眼前吧
奧格利聽了我的話

「⋯⋯最後的賭注麼？這種事情，我不討厭呢。這次就由我來當誘餌吧」
「沒關係麼？」
「嘛，死掉之前，我會全力逃走的，那時候就原諒我吧。不過，在那之前，我可是打算好好活躍一番呢」
「那就拜託了，不要勉強自己。我就算被踩扁了，也不會輕易死去」

雖然也不會那麼輕鬆就是了

不知道我能夠從被踩扁的狀態下恢復多少次
回想起馬路特迷宮的吸血鬼，到達極限之前，應該會有預兆的吧⋯⋯

嘛，兩邊都很危險，但也只能互相鼓勵，一起加油了

我和奧格利下定了決心
雖然在這之前早就下定過決心了