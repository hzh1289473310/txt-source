“嗯~”

可愛的呻吟聲傳入耳中，微弱的呼吸打在我的臉上，讓我覺得有點癢。

睜開眼睛，眼前的是天使，不，妖精的睡顏。

是莉莉那天真無邪的睡臉，可愛到讓我難以相信我竟然和她進行過死鬥。

“……已經到早上了嗎？”

從隔著窗簾射入房間的明亮光線，告訴我現在已經是日出時分了。

我躺在寬敞的床上，在雖然很簡樸，但被裝飾得很舒適的臥室內，雖然我還沒能習慣……但這裡就是我現在的家。

---

命運的初火之月已經結束了，今天是嶄新的紅炎之月1號。我們從神滅領域阿瓦隆回到了斯巴達。


妮露和賽利斯就直接在最初轉移到的首都阿瓦隆和我們分開了了。用於轉移的《歷史的開端》（零之編年史）設在神滅領域阿瓦隆入口處的黑色大正門附近，坍塌的神殿廢墟的地下室中。


莉莉平時進行採購時使用的轉移之處位於阿瓦隆附近的山裡面，但我必須去接在入口處等著我的梅麗。

因此，才用了這種繞路的方法順利地離開了迷宮，坐上在入口處等著我們的各自的坐騎，向著首都阿瓦隆出發。


我對讓梅麗等了相當長的時間稍感抱歉。

總之，只要不用再走一趟迷宮，後面的路途就沒有任何問題。我們悠閑地，不急不忙的，踏上了前往斯巴達的歸途。


但是，並不是所有人都平安回去了。

---

首先，西蒙沒回。

“不要，我絕對不會回去！這種寶山擺在眼前，怎麼可能回去啊！”

他一邊以非常興奮的叫著，一邊在香格里拉內跑來跑去。看來天空戰艦這個古代遺物，讓鍊金術師的探究心和好奇心熊熊燃燒了起來。

“這樣也沒關係吧？反正他隨時都可以回來，讓人造人照顧一下他就行了”

根據莉莉乾脆的判斷，西蒙遵照他本人的意願留在了香格里拉。他就這麼直接拋下剛剛開始運行的步槍工廠，和‘槍神’的成員不管了嗎？

嘛，等西蒙擺弄這些東西到滿足後，他自然就會回斯巴達的吧。


在這次戰鬥中，最開始就是西蒙讓我重新振作起來的，在最後關頭，也是西蒙用恢復彈幫助了我，在我向他表達了我那深深的感謝之情後，他同時也為我平安奪回莉莉而感到高興……男性朋友，不就是這種存在嗎。西蒙認為自己的工作已經結束了，現在他熱衷於香格里拉裡的古代技術。


雖然我還想再繼續和他這個摯友分享我的喜悅和成就感，但是繼續這麼做的話，也太婆婆媽媽了。

因為這種原因，西蒙就留在了香格里拉，,不過，還有一個人沒回斯巴達。

“啊，這麼說來，路多拉他去哪兒了啊？”

我注意到的時，已經是靠轉移離開迷宮，騎上瑪麗出發之後的事了。


他也在和莉莉的戰鬥中身受重傷，雖然說不上完全治癒了，但恢復到了傷勢不會再多行動產生影響的程度了，在全員進行轉移之時，我們肯定還是在一起的……但不知什麼時候，路多拉就不見了。

“我覺得沒必要去在意他”

“嗯，那傢伙大概已經去進行下一場戰鬥了吧”

法爾基斯和凱以十分了解路多拉的表情斷言道。

但對我來說，別說是給路多拉完成任務的報酬了，連感謝的話語都沒能對他說……但是，戰鬥一結束，就馬上隱匿行踪的行徑，也的確很符合他的風格。

那麼，就這樣吧。今後一定還能和他重逢的。雖然沒有任何根據，但我不可思議的確信著這點。

“嗯，黑乃……”

“早上好，莉莉”

“嗯，早啊~”

因為我離開床的舉動，莉莉也醒了過來。她稍顯睏倦的揉著她那大眼睛，又在著到我後露出了微笑。

果然，和莉莉一起睡的話就會感到很安心。不管是睡著之時，還是這種醒來之時。一切都沒有改變——我之所以會這麼覺得，果然是因為，還沒和和莉莉，做那個吧。


自從在維生艙中醒來時的對話以後，莉莉再也沒有提起過那個話題，而我更沒有提。


姑且約定為，莉莉和菲奧娜每天交換著和我睡覺，嘛，所以每晚都有著能她兩好好地兩人獨處的機會……不，不要再深思了，今後，我們可以堂堂正正地永遠在一起。還有很多時間。就算著急，也對這事沒用。


我一邊這麼讓自己別介意，但又非常的在意，一邊與莉莉一起做著起床的打理。

“早 上 好 啊！主人大人！！”

在我打理完後走到走廊，這麼向我精神飽滿的打招呼的人，是我家的女僕長小柩。


她自稱是真正的身姿，但這其實只是虛假的史萊姆之身，但小柩確實變成了可愛的有著超長黑髮的幼女，她早就穿上了與她那新身體尺寸相符的女僕裝，在宅邸中從東到西，從早到晚跑來跑去。

“米麗雅，你也早上好。”

“早上好”

以冰冷的機械聲回復我的，是今天也身著女僕裝的暴君之鎧。

明明小柩已經沒在裡面了……但不知為何，這傢伙現在也穿著這個特別定做的鎧甲用女僕裝，跟在小柩後面給她幫忙。


米麗雅，你曾經是國王吧？


不過，因為她喜歡這麼做，所以我也不會強制阻止她……但要是被人以為是我穿著鎧甲和女僕裝就頭疼了啊。

小柩和米麗雅的組合不顧我的煩惱，吧嗒吧嗒地向走廊另一側跑去。從一大早起就很有精神啊。話說，你們有在好好工作嗎？

---

“早上好，主人大人”

“早上好，莉莉大人”

當我們為了吃早飯來到餐廳時，還有兩張陌生的面孔恭恭敬敬地低著頭。


執事與女僕。年紀一樣是大約是20出頭年輕人。無論哪一個都容貌端正，都有著白膚紅瞳，以及，白髮。

是的，兩人都是從香格里拉帶過來的，人造人男女。

他兩的名字分別是，執事是M-0010，女僕是F-0001。但莉莉，這不是名字，這只是製造編號啊……

對於莉莉來說，這兩人可能只是個方便的工具罷了，但在我看來，人造人是擁有著能夠溝通的智慧的優秀人類。而且，既然像這樣生活在同一個屋子裡的話，那麼至少也該有個正式的名字。


所以，管家是塞巴斯蒂亞諾，女僕是羅登邁爾，我給他兩取了感覺似乎在哪裡聽到過的名字。

實際上，守護宅邸和照顧我們，只需小柩和沙利葉兩個人就夠了，她們並沒忙到需要再加兩個人幫忙。


莉莉特意把兩個人帶到宅邸中，是為了給自己的工作幫忙。因為莉莉還沒有放棄香格里拉和迪士尼樂園的支配權。

據說她今後也要開始在斯巴達研究古代魔法及其遺產。這兩個人就是為此的助手。


另外，在我們全部成員要去做任務的場合下，這兩人也可以看家。


順便一提，雖然號碼分別是10和1，但他們都是在香格里拉最初製造出來的個體。他們因為活著的時間最長而得到了成長，似乎是莉莉的人造人兵團中最優秀的人造人。但是，我還沒有無法區分長相一樣的人造人。

“早上好，master，莉莉大人”

“黑乃桑，莉莉桑，早上好。”

然後，沙利葉在廚房之中，而菲奧娜老早就坐到了餐廳裡。

沙利葉似乎挺中意在ディスティニー城堡裡穿的迷你女僕裝，現在還穿著這件衣服。與此相對的菲奧娜的裝扮就像是在冒險者公會的旅館裡一樣，十分隨便。

“不好意思，讓你久等了嗎？”

“不，沒有”

但看起來菲奧娜似乎已經吃了兩片吐司了。但這點東西對她來說還算不上是早餐吧。

而且，雖然我們住在貴族區，但畢竟我們可是冒險者隊伍。不用在意禮儀啊這種東西，應該按照我們自己的喜好來生活啊。


這才是我最想奪回的東西——我們的日常。

---

“打擾您一下，主人大人”

當我沉浸在這世界之時，突然傳來了人造人女僕的聲音。


突然被完全不熟悉的白膚美女羅登邁爾叫住的話，我稍微有點緊張。但不是對異性有意識的意思，而是升起了將她視為敵人的警戒心的意思。

“怎麼了，羅登？”

“有客人來訪。”

居然有人這麼早就來我家拜訪啊。是誰啊，我完全沒有頭緒。

“他自稱他是斯巴達軍第一隊‘勇敢的心’的中隊長，名叫騎士・埃里伍德・梅德里克斯。”

說起《勇氣的心》的埃里伍德，他是在加拉哈德之戰中，與琳菲露德戰鬥時被我救下的精靈壯漢。那時候他是指揮『角闘士』的副隊長,不過，他原本的職位是中隊長啊？


雖然我覺得這已經是很久以前的事了，但對他來說，他是終於處理完了加拉哈德之戰的後續事情後，才總算有時間來向救他一命的我道謝了吧。

雖然是在早到我都沒吃早飯的，不怎麼好的時間，但是也沒理由趕他回去。

“知道了，帶我過去吧。”

“yes，my lord”

在人造人同時發出遵命的話語後，作為女僕的羅登安靜地退下了。

“——好久不見，埃里伍德桑”

在我換好用來迎接客人的衣服之後，與在客廳等候的埃里伍德桑，如字面意思般，久違的重逢了。

“恩，這是自加拉哈德之戰爭以來和你第一次見面啊。好久不見，很高興見到你。我本來想早點來，可你老是不在斯巴達啊”

“不好意思，我有點忙。”

“哈哈哈，我聽說你很活躍……”

戰爭結束後，我在開拓村生活了一段時間，回來後，莉莉哭著離開了我，與菲奧娜交往，大戰混沌熊兔，最後又和莉莉以及菲奧娜進行了一場錯綜複雜的戀愛大戰。與其說忙，不如說在這短短時間內我經歷了多少次生死危機啊。比起加拉哈德之戰，更頻繁的遇見生命危機。


但是，這種辛苦的話就不用說了。只需相關人員了解這些情況就夠了。

“——可是……”

從加拉哈德之戰的回憶，討到斯巴達的戰後處理、以及在塞涅內討伐混沌熊兔的等等事件後，埃里伍德終於以銳利的目光說出了正事。

“關於相親一事，我想決定下準確日期。”

“啊！？”

雖然我是隱約覺得難道說……但沒想到，他真要說相親一事啊。

“啊—，關於這件事——”

“等下！無需多言。我也早就下定決心了”

你那決心，難道是要將女兒嫁出去的決心嗎？

但我是想說我到底會不會去相親啊，感覺彼此說話的前提條件完全相左，這是不是我的錯覺啊？

“所以，你只需去見我的女兒就好。”

雖然你擺著一副似乎像是面對著絕對無法逃避的悲哀宿命的表情，我也無法坦率的點頭稱是啊。

“之後的事情……哎，就讓你們兩個年輕人看著辦吧”

對於流露出仿彿接受了一切事情的氣氛的埃里伍德，我更無法說出強烈的反對之語。

雖然心裡會很難受，但在這裡拒絕也並不是什麼難事吧。


不過他既然如此的堅持，正是因為他想報答我吧。完全無視他的想法，直接拒絕他……這也太不像是個男人了吧

而且，現在已經沒有必要對這種話感到迷茫了。因為我已經心有所屬了。


我已經得到了莉莉和菲奧娜兩人。不能再貪求更多了。我已經有所愛之人了。再沒有比這更好的拒絕之語了吧。

因此，這裡應該暫且先接受相親，讓埃里伍德桑如願，這才是成熟的應對舉措。


莉莉和菲奧娜，也應該明白我的想法。不管怎麼說，她們都不可能誤會我想再對新的女性下手吧。

“我知道了。我就接受相親一事吧”

雖然我堂堂正正地接受了這事……

“誒誒！？黑乃不要啊！”

“沒想到這麼快就對新的女人出手了啊”

莉莉哭了出來，菲奧娜則冷眼瞪著我。


額，難道是我選錯了嗎？


看來我想重回那安靜和平的日常，還得再花上些時間。